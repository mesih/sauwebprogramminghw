﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web1.Models;

namespace web1.Controllers
{
    public class ArticleController : Controller
    {
        // GET: Article
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowArticle(int id)
        {
            web1Context db = new web1Context();

            List<article> articles = db.articles.Where(s => s.articleID == id).ToList();

            return PartialView(articles);
        }

        public ActionResult ShowComments(int articleID)
        {
            web1Context db = new web1Context();

            List<comment> comments = db.commetns.Where(s => s.article.articleID == articleID).ToList();

            return PartialView(comments);

        }
    }
}