﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web1.Models;

namespace web1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowArticles()
        {
            web1Context db = new web1Context();

            List<article> articles = db.articles.OrderByDescending(s => s.article_creation_time).ToList();

            return PartialView(articles);
        }
    }
}