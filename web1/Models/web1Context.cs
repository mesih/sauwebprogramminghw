﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace web1.Models
{
    public class web1Context : DbContext
    {
        public DbSet<category> categories { get; set; }
        public DbSet<article> articles { get; set; }
        public DbSet<member> members { get; set; }
        public DbSet<comment> commetns { get; set; }
    }
}