﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace web1.Models
{
    public class article
    {
        public int articleID { get; set; }

        [Required(ErrorMessage = "Please enter content of article.")]
        [DataType(DataType.Html)]
        public string article_data { get; set; }
        
        [Required(ErrorMessage = "Please enter header of article.")]
        public string article_header { get; set; }

        [Required(ErrorMessage = "Please enter creation time of article.")]
        [DataType(DataType.DateTime)]
        public DateTime article_creation_time { get; set; }

        public virtual member member { get; set; }
        public virtual List<comment> comments { get; set; }
    }
}