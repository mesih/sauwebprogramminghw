﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace web1.Models
{
    public class comment
    {
        public int commentID { get; set; }

        [Required(ErrorMessage = "Please enter content of comment.")]
        public string comment_data { get; set; }

        [Required(ErrorMessage = "Please enter creation time of comment.")]
        [DataType(DataType.DateTime)]
        public DateTime comment_creation_time { get; set; }

        public virtual article article { get; set; }
        public virtual member member { get; set; }
    }
}