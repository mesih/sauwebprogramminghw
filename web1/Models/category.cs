﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace web1.Models
{
    public class category
    {
        public int categoryID { get; set; }

        [Required(ErrorMessage = "Please enter name of category.")]
        [StringLength(50, ErrorMessage = "Name can not be longer than 50 character")]
        public string category_name { get; set; }

        public virtual List<article> articles { get; set; }
    }
}