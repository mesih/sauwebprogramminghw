﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace web1.Models
{
    public class member
    {
        public int memberID { get; set; }

        [Required(ErrorMessage = "Please enter email address.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please use a correct e-mail address.")]
        public string member_email { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        [StringLength(15, ErrorMessage = "Password can not be longer than 15 character")]
        public string member_password { get; set; }

        public virtual List<article> articles { get; set; }
        public virtual List<comment> comments { get; set; }
    }
}